package com.example.practica_radiobutton;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText et1, et2;
    private TextView tv1;
    private RadioButton rbt1, rbt2, rbt3, rbt4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et1 = (EditText)findViewById(R.id.txt_val1);
        et2 = (EditText)findViewById(R.id.txt_val2);
        tv1 = (TextView)findViewById(R.id.textView);
        rbt1 = (RadioButton)findViewById(R.id.rb_suma);
        rbt2 = (RadioButton)findViewById(R.id.rb_resta);
        rbt3 = (RadioButton)findViewById(R.id.rb_multiplicacion);
        rbt4 = (RadioButton)findViewById(R.id.rb_division);
    }

    //Metodo para el boton Calcular
    public void Calcular (View view){
        String Valor1_String = et1.getText().toString();
        String Valor2_String = et2.getText().toString();

        int valor1_int = Integer.parseInt(Valor1_String);
        int valor2_int = Integer.parseInt(Valor2_String);

        if (rbt1.isChecked() == true){
            int suma = valor1_int + valor2_int;
            String resultado = String.valueOf(suma);
            tv1.setText(resultado);
        }else if (rbt2.isChecked() == true){
            int resta = valor1_int - valor2_int;
            String resultado = String.valueOf(resta);
            tv1.setText(resultado);
        }else if (rbt3.isChecked() == true){
            int multiplicacion = valor1_int * valor2_int;
            String resultado = String.valueOf(multiplicacion);
            tv1.setText(resultado);
        }else if (rbt4.isChecked() == true){

            if (valor2_int != 0){
                int division = valor1_int / valor2_int;
                String resultado = String.valueOf(division);
                tv1.setText(resultado);
            }else{
                Toast.makeText(this,"El segundo valor debe ser diferente de o", Toast.LENGTH_LONG).show();
            }
        }
    }
}
